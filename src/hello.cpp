#include <iostream>
#include "rand.h"
#include "parser.h"

int main()
{
    mylib::RandDouble rd;

    mylib::Parser parser("./config.cfg");

    int size = parser.getInt("size");
    int degree = parser.getInt("degree");

    std::cout << size << std::endl;
    std::cout << degree << std::endl;
    

    return 0;
}
