CPPFLAGS = -Iinclude -I/Users/kevinyoung/working/mycpplib/include -L/Users/kevinyoung/working/mycpplib/lib -Wall -Wextra
SOURCE = $(wildcard src/*.cpp)
TARGET = $(patsubst %.cpp,%,$(SOURCE))

all: $(TARGET)
	./$(TARGET)

$(TARGET): $(SOURCE)
	$(CXX) $(CPPFLAGS) $< -o $@ -lmycpp

clean:
	rm -f $(TARGET)
